module "development" {
  source     = "./environment"
  kubeconfig = var.kubernetes_config.dev.donneesetterritoires-development
  namespace  = "donneesetterritoires-development"

  gitlab_environment_scope = "development"
  base_domain              = var.dev_base_domain
  grist_form_base_domain   = "formulaires.${var.dev_base_domain}"

  gitlab_project_ids = local.gitlab_project_ids
  monitoring_org_id  = random_string.development_secret_org_id.result
}

module "production" {
  source     = "./environment"
  kubeconfig = var.kubernetes_config.prod.donneesetterritoires
  namespace  = "donneesetterritoires"

  gitlab_environment_scope = "production"
  base_domain              = var.prod_base_domain

  gitlab_project_ids = local.gitlab_project_ids
  monitoring_org_id  = random_string.production_secret_org_id.result

  grist_form_base_domain     = var.production_grist_form_base_domain
  grist_form_db_path         = var.production_grist_form_db_path
  grist_form_grist_api_token = var.production_grist_form_grist_api_token
  grist_form_grist_url       = "https://grist.incubateur.anct.gouv.fr/"
}

resource "scaleway_object_bucket" "umap_dev_backups" {
  provider = scaleway.project
  name     = "${var.project_slug}-umap-dev-backups"
}
module "umap_development" {
  source     = "./environment_umap"
  kubeconfig = var.kubernetes_config.dev.donnees-umap
  namespace  = "donnees-umap"

  gitlab_environment_scope = "development"
  project_id               = local.gitlab_project_ids.umap
  base_domain              = "umap.dev.incubateur.anct.gouv.fr"
  backup_bucket_name       = scaleway_object_bucket.umap_dev_backups.name
  backup_bucket_access_key = var.scaleway_project_config.access_key
  backup_bucket_secret_key = var.scaleway_project_config.secret_key
  backup_bucket_endpoint   = "https://s3.fr-par.scw.cloud"
  backup_bucket_region     = "fr-par"
  sso_endpoint             = var.development_tools_umap_sso_endpoint
  sso_key                  = var.development_tools_umap_sso_key
  sso_secret               = var.development_tools_umap_sso_secret
  monitoring_org_id        = random_string.development_secret_org_id.result
}

resource "scaleway_object_bucket" "umap_prod_backups" {
  provider = scaleway.project
  name     = "${var.project_slug}-umap-prod-backups"
}
module "umap_production" {
  source     = "./environment_umap"
  kubeconfig = var.kubernetes_config.prod.donnees-umap
  namespace  = "donnees-umap"

  gitlab_environment_scope = "production"
  project_id               = local.gitlab_project_ids.umap
  base_domain              = "umap.incubateur.anct.gouv.fr"
  backup_bucket_name       = scaleway_object_bucket.umap_prod_backups.name
  backup_bucket_access_key = var.scaleway_project_config.access_key
  backup_bucket_secret_key = var.scaleway_project_config.secret_key
  backup_bucket_endpoint   = "https://s3.fr-par.scw.cloud"
  backup_bucket_region     = "fr-par"
  sso_endpoint             = var.production_tools_umap_sso_endpoint
  sso_key                  = var.production_tools_umap_sso_key
  sso_secret               = var.production_tools_umap_sso_secret
  monitoring_org_id        = random_string.production_secret_org_id.result
}

module "catalog_development" {
  source     = "./environment_catalog"
  kubeconfig = var.kubernetes_config.dev.donnees-catalog-development
  namespace  = "donnees-catalog-development"

  gitlab_environment_scope = "development"
  project_id               = local.gitlab_project_ids.catalog
  base_domain              = "catalogue-indicateurs.${var.dev_base_domain}"
  monitoring_org_id        = random_string.development_secret_org_id.result
}

module "catalog_production" {
  source     = "./environment_catalog"
  kubeconfig = var.kubernetes_config.prod.donnees-catalog
  namespace  = "donnees-catalog"

  gitlab_environment_scope = "production"
  project_id               = local.gitlab_project_ids.catalog
  base_domain              = "catalogue-indicateurs.${var.prod_base_domain}"
  monitoring_org_id        = random_string.production_secret_org_id.result
}
