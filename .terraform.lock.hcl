# This file is maintained automatically by "tofu init".
# Manual edits may be lost in future updates.

provider "registry.opentofu.org/gitlabhq/gitlab" {
  version     = "3.12.0"
  constraints = "~> 3.12.0"
  hashes = [
    "h1:zZKpUsf1STvLLyH30a6ZcjQoARU8MJGbHsGCjYHrnOA=",
    "zh:1c3e89cf19118fc07d7b04257251fc9897e722c16e0a0df7b07fcd261f8c12e7",
    "zh:207f7ffaaf16f3d0db9eec847867871f4a9eb9bb9109d9ab9c9baffade1a4688",
    "zh:2360bdd3253a15fbbb6581759aa9499235de670d13f02272cbe408cb425dc625",
    "zh:2a9bec466163baeb70474fab7b43aababdf28b240f2f4917299dfe1a952fd983",
    "zh:3a6ea735b5324c17aa6776bff3b2c1c2aeb2b6c20b9c944075c8244367eb7e4c",
    "zh:3aa73c622187c06c417e1cd98cd83f9a2982c478e4f9f6cd76fbfaec3f6b36e8",
    "zh:51ace107c8ba3f2bb7f7d246db6a0428ef94aafceca38df5908167f336963ec8",
    "zh:53a35a827596178c2a12cf33d1f0d0b4cf38cd32d2cdfe2443bdd6ebb06561be",
    "zh:5bece68a724bffd2e66293429319601d81ac3186bf93337c797511672f80efd0",
    "zh:60f21e8737be59933a923c085dcb7674fcd44d8a5f2e4738edc70ae726666204",
    "zh:9fb277f13dd8f81dee7f93230e7d1593aca49c4788360c3f2d4e42b8f6bb1a8f",
    "zh:ac63a9b4a3a50a3164ee26f1e64cc6878fcdb414f50106abe3dbeb7532edc8cd",
    "zh:ed083de9fce2753e3dfeaa1356508ecb0f218fbace8a8ef7b56f3f324fff593b",
    "zh:ed966026c76a334b6bd6d621c06c5db4a6409468e4dd99d9ad099d3a9de22768",
  ]
}

provider "registry.opentofu.org/grafana/grafana" {
  version     = "1.27.0"
  constraints = "~> 1.27.0"
  hashes = [
    "h1:ifKgRrZHspeXMSavSCsWbLrk6tC1NOyWN0iiGv2j6I4=",
    "zh:01ef0ae20530a54cbb4bffc35e97733916e5ae2e8f7fa00aefa2e86e24206823",
    "zh:08a4ac8b690bab9a3b454c3d998917f4ed49fc225a21ff53ceb0488eb4b9d15d",
    "zh:0e08516cd6c2495bc83a4a8e0252bfa70e310aa400af0fe766bbe7ddd05a21cb",
    "zh:14856865f6e6695e6d7708d70844a2c031cfc9b091e7cf530a453b2f78c9a691",
    "zh:2b1c05fff5011ab83acdd292484857fe886cd113abbb7fc617bbb8f358517cc0",
    "zh:31bae1b1c635a94329470b30986d336f4b3819bf24aacd953d5b57debb83bd4d",
    "zh:352b6ea190711c8f3f107540c8943c8f6b9faf4fbc73a9c1721b15db4a103edb",
    "zh:7eda29d30d451b842c5b0b2cf15cb907e76e8bac4843e90830a62a68bbe877a5",
    "zh:bd640d7e8a126d810a34766816b4e17a07c634ffef14b468269c8191683fff27",
    "zh:ddfa43a7b31fb840f04420c82fe0313a44fa5099c3d1f61219e630d6c8440e2d",
    "zh:e50dccaf8cb9922ac25e2f87a85083d5c2cef5323eac4ce7d933012af7a25e88",
    "zh:e72903aeb4830b7b89efcf7336a61c736d9049c4156b6f17cec51663ed6e803d",
    "zh:f4161d62960ec9f9d84cb73437a9b9195831c467cdcc3381e431fa6e2cd92a14",
    "zh:f4699da872dfc9847eb3da49fd6ae4943e92602b617931bb07b91e646d90a279",
  ]
}

provider "registry.opentofu.org/hashicorp/helm" {
  version     = "2.14.0"
  constraints = "~> 2.0"
  hashes = [
    "h1:ibK3MM61pVjBwBcrro56OLTHwUhhNglvGG9CloLvliI=",
    "zh:1c84ca8c274564c46497e89055139c7af64c9e1a8dd4f1cd4c68503ac1322fb8",
    "zh:211a763173934d30c2e49c0cc828b1e34a528b0fdec8bf48d2bb3afadd4f9095",
    "zh:3dca0b703a2f82d3e283a9e9ca6259a3b9897b217201f3cddf430009a1ca00c9",
    "zh:40c5cfd48dcef54e87129e19d31c006c2e3309ee6c09d566139eaf315a59a369",
    "zh:6f23c00ca1e2663e2a208a7491aa6dbe2604f00e0af7e23ef9323206e8f2fc81",
    "zh:77f8cfc4888600e0d12da137bbdb836de160db168dde7af26c2e44cf00cbf057",
    "zh:97b99c945eafa9bafc57c3f628d496356ea30312c3df8dfac499e0f3ff6bf0c9",
    "zh:a01cfc53e50d5f722dc2aabd26097a8e4d966d343ffd471034968c2dc7a8819d",
    "zh:b69c51e921fe8c91e38f4a82118d0b6b0f47f6c71a76f506fde3642ecbf39911",
    "zh:fb8bfc7b8106bef58cc5628c024103f0dd5276d573fe67ac16f343a2b38ecee8",
  ]
}

provider "registry.opentofu.org/hashicorp/kubernetes" {
  version     = "2.13.1"
  constraints = "~> 2.0, >= 2.13.0, ~> 2.13.0, ~> 2.13"
  hashes = [
    "h1:fY2gWOd+w5CKV+B+T9LugeprJxHjjkaW81QZ4Urpzlk=",
    "zh:15d61e1ea6e428437e5947330a8328ed62b61bca01e0f2584e4e80d0aaea93ab",
    "zh:61183fddab8988a892f459abb38d9d6e50b8db0e2b252e398a10270e9a4f3328",
    "zh:67db11bf1596595ada2ae4e8fce13ac3e7ffd242ff8f6a2440bd8086c0d2cd09",
    "zh:6be96982825d821df7b29dc0c40c2f5807561e6af33e858668d1e06e1df3efc3",
    "zh:6e391b9601349ded72135e5f9159161f3cf9042e272611c2b5d863bda2724302",
    "zh:73dd61f4dc6f03f58c643a5dcee8170520f38ec06248186b651ea3d951ed7d6a",
    "zh:92b2f3f0657cfe0ad7cfba72e89246f5d02be53389a56fc205716b549e4848cd",
    "zh:a7ff7812eebdcaf75bb8937acc742d8f7d796d2df93d2c2feb27c5813e548da1",
    "zh:ac2edd95b356ac0fb48569c825cb91dfc0820c688ef12cc657c2305b8f12be94",
    "zh:eabae340b652d63f8f127771e70af854671bf76014f8c53e1f95b09d42729e0d",
  ]
}

provider "registry.opentofu.org/hashicorp/null" {
  version     = "3.2.2"
  constraints = "~> 3.0"
  hashes = [
    "h1:xN1tSeF/rUBfaddk/AVqk4i65z/MMM9uVZWd2cWCCH0=",
    "zh:00e5877d19fb1c1d8c4b3536334a46a5c86f57146fd115c7b7b4b5d2bf2de86d",
    "zh:1755c2999e73e4d73f9de670c145c9a0dc5a373802799dff06a0e9c161354163",
    "zh:2b29d706353bc9c4edda6a2946af3322abe94372ffb421d81fa176f1e57e33be",
    "zh:34f65259c6d2bd51582b6da536e782b181b23725782b181193b965f519fbbacd",
    "zh:370f6eb744475926a1fa7464d82d46ad83c2e1148b4b21681b4cec4d75b97969",
    "zh:5950bdb23b4fcc6431562d7eba3dea37844aa4220c4da2eb898ae3e4d1b64ec4",
    "zh:8f3d5c8d4b9d497fec36953a227f80c76d37fc8431b683a23fb1c42b9cccbf8a",
    "zh:8f6eb5e65c047bf490ad3891efecefc488503b65898d4ee106f474697ba257d7",
    "zh:a7040eed688316fe00379574c72bb8c47dbe2638b038bb705647cbf224de8f72",
    "zh:e561f28df04d9e51b75f33004b7767a53c45ad96e3375d86181ba1363bffbc77",
  ]
}

provider "registry.opentofu.org/hashicorp/random" {
  version     = "3.5.1"
  constraints = "~> 3.5.1"
  hashes = [
    "h1:tW+G7lgqbHUtraKHPWuotYHlME1vcAf50YvOeHQlGHg=",
    "zh:0002dd4c79453da5bf1bb9c52172a25d042a571f6df131b7c9ced3d1f8f3eb44",
    "zh:49b0f8c2bd5632799aa6113e0e46acaa7d008f927665a41a1f8e8559fe6d8165",
    "zh:56df70fca236caa06d0e636c41ab71dd1ced05375f4ddcb905b0ed2105737048",
    "zh:58e4de40540c86b9e2e2595dac1318ba057718961a467fa9727866f747693eb2",
    "zh:5992f11c738812ccd7476d4c607cb8b76dea5aa612be491150c89957ec395ddd",
    "zh:7ff4f0b7707b51737f684e96d85a47f0dd8be0f72a3c27b0798755d3faad15e2",
    "zh:8e4b0972e216c9773ab525accfa36eb27c44c751b06b125ecc53f4226c91cea8",
    "zh:d8956cc5abcd5d1173b6cc25d5d8ed2c5cc456edab2fddb774a17d45e84820cb",
    "zh:df7f9eb93a832e66bc20cc41c57d38954f87671ec60be09fa866273adb8d9353",
    "zh:eb583d8f03b11f0b6c535375d8ed0d29e5f7f537b5c78943856d2e8ce76482d9",
  ]
}

provider "registry.opentofu.org/hashicorp/time" {
  version     = "0.11.2"
  constraints = ">= 0.7.0"
  hashes = [
    "h1:WVXqTGyMwPRVhT7R9DAMhTwdNaoAQeuGKqAncyS5yh8=",
    "zh:534d56fac2daa5c15737fa1907b7afe2d91474197a0ec272d3f864d2a0a87743",
    "zh:67c7765dc3b5ec19b6df32c29620de4b3e7846f5aa1a6b1b0e15394d87d0f875",
    "zh:9502412f028d17051ea0550f5015d0515da78bc938f415191d9171742481330c",
    "zh:982984971fe6cb87600f118fbf754e7f60f7ad343735a4f99d632bf148319cee",
    "zh:9b0bedf8781be6c414f314989246a3b9a79302845db57dbe497d768cd56ee73e",
    "zh:a35244d464dcad8940060c49b66a2f653d2dfc4e471c2dd59e2118a17a279427",
    "zh:c8e3534b8cdff62f88a9882aed8c6dc4484639662bff2ea4e89499f1209b9ba9",
    "zh:e78c88613bb6eab52d94bb7592d4df3a79bf69b9f1e50a86f9d9174db7e5251c",
    "zh:fb28ffd2b641a449bd5526bef6e547894b4c5ddac0cef05ee03881a6c53eac39",
    "zh:fcfcd35ff1178cbf11034e2eaf4254f9aadd8531f75d1b15b5e245715183d49b",
  ]
}

provider "registry.opentofu.org/scaleway/scaleway" {
  version     = "2.41.3"
  constraints = "~> 2.2"
  hashes = [
    "h1:QCPDxM0eKQ+BcV5KnZSYMIA2sFYYnTaNzZot1tZX5VY=",
    "zh:010f4b8e5a2a13961d16a7af7d9bc5ca779633ce1ef123c054406c4b68408f7c",
    "zh:0a97bd9bb974d8bf8d5abe951a3f5026e53a300f41a95cc0e274de669b72d67d",
    "zh:1b96e136e22936f044fbae57b78bc4edccfc87772bd494b308657121b7e4bad6",
    "zh:212ff93bff1e08cde5574d007f4211b9c704084af81347dc4eb641035902a53d",
    "zh:335acfa1490f6722a26063a3893e9925ab1320170c61c35e58d25ddc4da18429",
    "zh:3e2ac50a976ac67e0a30da4de10bfedbca506a23f8317a75cb574986cc8061d6",
    "zh:431e0cb2927dd4e3284d6df15507961a5ef51f1c2a39cdbab412051da8d0449c",
    "zh:56dd327aa6ca5b9f33b02b824132b478b551a8e73837a6e5b78a48800e3354e2",
    "zh:5b2d5d304e3e272a5d93dc4ce8b5813cbe2484950b2d7705fd04b43acfd2a125",
    "zh:724de80f424dad9c618a143df1a9b318b9188a0ac32c1cafac10660065c071f8",
    "zh:7bbaa4af9d202424a88e41f60415fd672c68582f400bc793d654f357b9724d48",
    "zh:d43b1cc8a7fd22ceb437f713491c15eb0a4902c967bcaa1cdc5fa55f0d99a718",
    "zh:e0dc9b61daf4e2040050313d866a54aecefcd33675c537b8ef341a476f184f33",
    "zh:effd715aa89d76052ecce983f4bc9436b3b95d66664542c04874942698f67089",
  ]
}
