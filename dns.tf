resource "scaleway_domain_record" "gitlab_pages" {
  provider = scaleway.project
  dns_zone = var.gitlab_pages_zone
  name     = ""
  type     = "A"
  data     = var.vm_ip
  ttl      = 60
}

resource "scaleway_domain_record" "gitlab_pages_wildcard" {
  provider = scaleway.project
  dns_zone = var.gitlab_pages_zone
  name     = "*"
  type     = "A"
  data     = var.vm_ip
  ttl      = 60
}
