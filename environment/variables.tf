variable "base_domain" {
  type = string
}
variable "grist_form_base_domain" {
  type = string
}

variable "gitlab_environment_scope" {
  type = string
}

variable "gitlab_project_ids" {
  type = object({
    zonage     = number
    grist_form = number
    umap       = number
  })
}

variable "monitoring_org_id" {
  type = string
}

variable "grist_form_grist_url" {
  type    = string
  default = ""
}
variable "grist_form_db_path" {
  type      = string
  default   = ""
  sensitive = true
}
variable "grist_form_grist_api_token" {
  type      = string
  default   = ""
  sensitive = true
}
