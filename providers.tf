provider "scaleway" {
  alias      = "project"
  region     = "fr-par"
  zone       = "fr-par-1"
  access_key = var.scaleway_project_config.access_key
  secret_key = var.scaleway_project_config.secret_key
  project_id = var.scaleway_project_config.project_id
}

# output "pyhelmdiff_kubeconfig_mapping" {
#   value = {
#     "provider[\"registry.terraform.io/hashicorp/helm\"].development" = local.kubeconfig_development
#     "provider[\"registry.terraform.io/hashicorp/helm\"].production"  = local.kubeconfig_production
#   }
#   sensitive = true
# }

provider "gitlab" {
  token = var.gitlab_token
}

provider "grafana" {
  alias  = "production"
  url    = var.grafana_production_config.url
  auth   = var.grafana_production_config.api_key
  org_id = var.grafana_production_config.org_id
}

provider "grafana" {
  alias  = "development"
  url    = var.grafana_development_config.url
  auth   = var.grafana_development_config.api_key
  org_id = var.grafana_development_config.org_id
}
