resource "kubernetes_ingress_v1" "security_txt" {
  metadata {
    name      = "security-txt"
    namespace = var.namespace
    annotations = {
      "cert-manager.io/cluster-issuer" = "letsencrypt-prod"
    }
  }

  spec {
    ingress_class_name = "haproxy"
    rule {
      host = var.domain
      http {
        path {
          path      = "/.well-known/security.txt"
          path_type = "Exact"
          backend {
            service {
              name = "security-txt"
              port {
                number = 80
              }
            }
          }
        }
      }
    }
  }
}

resource "kubernetes_service_v1" "security_txt" {
  metadata {
    name      = "security-txt"
    namespace = var.namespace
  }
  spec {
    selector = {
      "app.kubernetes.io/name" = "security-txt"
    }
    port {
      port        = 80
      target_port = 80
    }
  }
}

resource "kubernetes_deployment_v1" "security_txt" {
  metadata {
    name      = "security-txt"
    namespace = var.namespace
  }
  spec {
    selector {
      match_labels = {
        "app.kubernetes.io/name" = "security-txt"
      }

    }
    template {
      metadata {
        name      = "security-txt"
        namespace = var.namespace
        labels = {
          "app.kubernetes.io/name" = "security-txt"
        }
      }
      spec {
        container {
          name  = "nginx"
          image = "docker.io/nginx:1.26.1"
          port {
            container_port = 80
          }
          volume_mount {
            name       = "security-txt"
            mount_path = "/usr/share/nginx/html/.well-known"
          }
        }
        volume {
          name = "security-txt"
          config_map {
            name = "security-txt"
          }
        }
      }
    }
  }
}

resource "kubernetes_config_map_v1" "security_txt" {
  metadata {
    name      = "security-txt"
    namespace = var.namespace
  }
  data = {
    "security.txt" = <<-EOT
  Contact: mailto:support-incubateur@anct.gouv.fr
  Policy: https://incubateur.anct.gouv.fr/security-policy
  Preferred-Languages: fr, en
  Expires: 2024-12-01T00:00:00.000Z
  EOT
  }
}
