resource "kubernetes_secret_v1" "grist_secret" {
  metadata {
    namespace = var.namespace
    name      = "grist-secret"
  }
  data = {
    s3_access_key       = var.scaleway_project_config.access_key
    s3_secret_key       = var.scaleway_project_config.secret_key
    oauth_client_id     = var.oauth_client_id
    oauth_client_secret = var.oauth_client_secret
    redis_url           = "redis://default:${resource.random_password.redis_password.result}@${module.redis.hostname}"
  }
}
